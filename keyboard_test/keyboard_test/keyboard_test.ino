#include "Keyboard.h"
//Pin connected to ST_CP of 74HC595
int latchPin = 8;
//Pin connected to SH_CP of 74HC595
int clockPin = 7;
////Pin connected to DS of 74HC595
int dataPin = 9;
int inputs[3] = {4, 5, 6};
const byte inputsNum = 3;
unsigned int lastCombID = 0;
unsigned int fullCombID = 0;
//adg
const int analog0 = A0; 
const int analog1 = A1; 
bool shiftOn = false;
bool numOn = false;
bool shiftButtonPress = false;
bool numButtonPress = false;

bool _isUp = false;
bool _isDown = false;
bool _isLeft = false;
bool _isRight = false;

#define shiftLed 2
#define numLed 3

const unsigned int simbCodes[] = {
  1,9,2,18,130,4,36, 1026
};
const char simb[] = {
  ' ', '\n', '.', ',', ';', '-', '+', '?'
};
const unsigned int codes[] = {
  8,16,32,64,128,256,512,1024,2048,4096,8192,16384,72,144,288,520,1040,2080,4104,8208,16416,576,1152,2304,4160,8320,16640,4608,9216,18432,8200,16392,80
};

const char nums[] = {
  '1','2','3','4','5','6','7','8','9','0','*','/'
};
const char lathinLover[] = {
  'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'
};

byte rowDecode[] = {5,2,4,3,1,6,0,7};

byte leds = 0;
void setup(){
  Serial.begin(9600);
  //set pins to output so you can control the shift register
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  pinMode(shiftLed, OUTPUT);
  
  for(byte i = 0; i < inputsNum; i++)
  {
    pinMode(inputs[i], INPUT);
  }
  Keyboard.begin();
}

char FindCharByCombo(unsigned int combo)
{
  for(int i = 0; i < sizeof(simb); i++)
  {
    if(combo == simbCodes[i])
      return simb[i];
  }
  
  if (numOn)
  {
    for(int i = 0; i < (sizeof(nums)); i++)
    {
      if (combo == codes[i])
      {
        return nums[i];
      }
    }
  }
  else
  {
    for(int i = 0; i < (sizeof(lathinLover)); i++)
    {
      if (combo == codes[i])
      {
        char l = lathinLover[i];
        if (shiftOn)
          l -= 32;
        return l;
      }
    }
  }
  return '~';
}

void loop() {
  unsigned int combinationID = 0;
  for(int i = 0; i < 8; i++){
    leds = 0;
    bitSet(leds, i);
    digitalWrite(latchPin, LOW);
    shiftOut(dataPin, clockPin, LSBFIRST, leds);
    digitalWrite(latchPin, HIGH);
    delay(1);
    for(int j = 0; j < inputsNum; j++)
    {
      bool isButtonPressed = digitalRead(inputs[j]);
      byte buttonNum = rowDecode[i] * 3 + j;
      if (i == 0 && j == 0)
      {
        if (shiftButtonPress && !isButtonPressed)
          shiftOn = !shiftOn;
        digitalWrite(shiftLed, shiftOn);
        shiftButtonPress = isButtonPressed;
      }
      if (i == 5 && j == 2)
      {
        if (numButtonPress && !isButtonPressed)
          numOn = !numOn;
        digitalWrite(numLed, numOn);
        numButtonPress = isButtonPressed;
      }
      if (isButtonPressed)
      {
        if (rowDecode[i] < 5)
        {
          combinationID += 1 << buttonNum;
          fullCombID = fullCombID | (1 << buttonNum);
        }
        else
        {
          Serial.print(i);
          Serial.println(j);
        }
      }
    }
  }
  if (combinationID == 0 && lastCombID != 0)
  {
    char keyPress = FindCharByCombo(fullCombID);
    if (fullCombID == 65)
    {
      Keyboard.press(0xB2);
      Keyboard.release(0xB2);
    }
    else if (keyPress != '~')
    {
      Keyboard.print(FindCharByCombo(fullCombID));
    }
    Serial.println(fullCombID);
    fullCombID = 0;
  }
  lastCombID = combinationID;
  
//  Serial.println(fullCombID);
  int vert = analogRead(analog0);
  int hor = analogRead(analog1);
  // UP  


  if (vert < 10)
  {
      if (!_isUp)
      {
          Keyboard.press(0xDA);
          _isUp = true;
      }
  }
  else if (_isUp)
  {
      Keyboard.release(0xDA);
      _isUp = false;
  }
  // DOWN
  if (vert > 1000)
  {
      if (!_isDown)
      {
          Keyboard.press(0xD9);
          _isDown = true;
      }
  }
  else if (_isDown)
  {
      Keyboard.release(0xD9);
      _isDown = false;
  }

//----------------------------------
// left
  if (hor < 10)
  {
      if (!_isLeft)
      {
          Keyboard.press(0xD8);
          _isLeft = true;
      }
  }
  else if (_isLeft)
  {
      Keyboard.release(0xD8);
      _isLeft = false;
  }
  // Right
  if (hor > 1000)
  {
      if (!_isRight)
      {
          Keyboard.press(0xD7);
          _isRight = true;
      }
  }
  else if (_isRight)
  {
      Keyboard.release(0xD7);
      _isRight = false;
  }
  delay(10);
}
