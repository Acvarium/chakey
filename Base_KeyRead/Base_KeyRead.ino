byte rows[] = {2,3,4};
const int rowCount = sizeof(rows)/sizeof(rows[0]);


byte cols[] = {5,6,7,8,9};
const int colCount = sizeof(cols)/sizeof(cols[0]);

byte keys[colCount][rowCount];

void setup() {
  Serial.begin(115200);
  delay(3000);
  for(int x=0; rowCount; x++) {
      Serial.print(rows[x]); Serial.println(" as input");
      pinMode(rows[x], INPUT);
  }

  for (int x=0; colCount; x++) {
      Serial.print(cols[x]); Serial.println(" as OUTPUT");
      pinMode(cols[x], OUTPUT);

  }
}


void readMatrix() {
    
    // iterate the columns
    for (int colIndex=0; colIndex < colCount; colIndex++) {
        // col: set to output to low
        byte curCol = cols[colIndex];
        digitalWrite(curCol, LOW);
    }
    for (int colIndex=0; colIndex < colCount; colIndex++) {
        // col: set to output to low
        byte curCol = cols[colIndex];
        digitalWrite(curCol, HIGH);
        delay(1);
        for (int rowIndex=0; rowIndex < rowCount; rowIndex++) {
            byte rowCol = rows[rowIndex];
            keys[colIndex][rowIndex] = digitalRead(rowCol);
        }
        digitalWrite(curCol, LOW);
        // disable the column
    }
}

void printMatrix() {
    for (int rowIndex=0; rowIndex < rowCount; rowIndex++) {
        if (rowIndex < 10)
            Serial.print(F("0"));
        Serial.print(rowIndex); Serial.print(F(": "));
 
        for (int colIndex=0; colIndex < colCount; colIndex++) {  
            Serial.print(keys[colIndex][rowIndex]);
            if (colIndex < colCount)
                Serial.print(F(", "));
        }   
        Serial.println("");
    }
    Serial.println("");
}
void loop() {
    readMatrix();
    printMatrix();
    delay(20);
}
